<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class VehicleController extends Controller
{

    private $client;
    private $base_url;

    public function __construct(){
        $this->client = new Client();
        $this->base_url = 'https://one.nhtsa.gov/webapi/api/SafetyRatings/';
    }

    /**
     * @param Request $request
     * @param $modelYear
     * @param $manufacturer
     * @param $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVehicles(Request $request, $modelYear, $manufacturer, $model)
    {
        try{
            $url =  $this->base_url.'modelyear/'
                .$modelYear.'/make/'
                .$manufacturer.'/model/'
                .$model.'?format=json';

            $response = $this->client->request('GET', $url);
            $results = json_decode( $response->getBody() );

            $collection = collect( $results->Results )->map( function ($result) use ($request) {

                $vehicle = [];

                if( $request->has('withRating') && json_decode($request->withRating) ){
                    $vehicle["CrashRating"] = $this->getCrushRatings( $result->VehicleId );
                }

                $vehicle["Description"] = $result->VehicleDescription;
                $vehicle["VehicleId"] = $result->VehicleId;

                return $vehicle;

            })->reject(function ($result) {
                return empty($result);
            });


            return response()->json([
                "Count" => (int)$results->Count,
                "Results" => $collection
            ]);

        }catch (\Exception $e){

            return [
                "Count" => 0,
                "Results" => []
            ];
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postVehicles(Request $request)
    {
        try{
            $content = json_decode( $request->getContent() );

            $model = array_key_exists('model',$content) && !empty($content->model) ? $content->model : 'undefined';
            $modelYear = array_key_exists('modelYear',$content) && !empty($content->modelYear) ? $content->modelYear : 'undefined';
            $manufacturer = array_key_exists('manufacturer',$content) && !empty($content->manufacturer) ? $content->manufacturer : 'undefined';

            return $this->getVehicles( $request, $modelYear, $manufacturer, $model);

        }catch (\Exception $e){

            return [
                "Count" => 0,
                "Results" => []
            ];
        }
    }


    /**
     * @param $vehicleId
     * @return mixed
     */
    private function getCrushRatings( $vehicleId )
    {
        try{
            $url =  $this->base_url."VehicleId/"
                . $vehicleId ."?format=json";

            $response = $this->client->request('GET', $url);
            $results = json_decode( $response->getBody() );

            return collect( $results->Results )->first()->OverallRating;
        }catch (\Exception $e){

            return null;
        }

    }
}
