# App configs. #

You may need to change a few serve configuration to get the application to work.

### PHP version 7.1 ###

### App Configs ###
* Cloning the app at the serve web root and run $ composer install.
* * Incase of an error (Mac and Ubuntu) try $ sudo composer install

### Apache Server configs ###
* Open apache httpd.conf
* Change directory root to point to applications public folder

### DocumentRoot "/web_root_path/moduscreateapitest/public" ###
### <Directory "/web_root_path/moduscreateapitest/public"\> ###

* Restart web server
* APIs should be ready for access.
